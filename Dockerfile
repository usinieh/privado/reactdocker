FROM node:14
MAINTAINER vhuezo <huezohuezo.1990@gmail.com>
ENV TZ=America/El_Salvador
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /app
COPY . .
RUN ls -ltra /app/
RUN  npm i 
CMD  npm start
EXPOSE 3000

